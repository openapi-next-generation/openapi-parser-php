<?php

namespace OpenapiNextGeneration\OpenapiParserPhp;

class JsonParser implements ParserInterface
{
    public static function parse(string $specification): array
    {
        return json_decode($specification, true);
    }
}