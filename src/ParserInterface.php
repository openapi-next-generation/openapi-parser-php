<?php

namespace OpenapiNextGeneration\OpenapiParserPhp;

interface ParserInterface
{
    /**
     * Method to parse a given api specification (e.g. imported from a file) and return it in
     * a standartized PHP array so it an be further processed
     */
    public static function parse(string $specification): array;
}