<?php

namespace OpenapiNextGeneration\OpenapiParserPhp;

use Symfony\Component\Yaml\Yaml;

class YamlParser implements ParserInterface
{
    public static function parse(string $specification): array
    {
        return Yaml::parse($specification);
    }
}